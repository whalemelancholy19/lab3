#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

bool equality(int** matrix_a, int lines_a, int columns_a, int** matrix_b, int lines_b, int columns_b)
{
     bool check = true;

     if ((lines_a != lines_b) || (columns_a != columns_b))
     {
         return false;
     }
     else
     {
        for (int j = 0; j < columns_a; j++)
        {
            for (int i = 0; i < lines_a; i++)
            {
                if (matrix_a[i][j] != matrix_b[i][j])
                {
                    check = false;
                }
            }
        }
     }

     return check;
}

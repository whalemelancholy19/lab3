#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** sudt(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b, int choice5)
{
    int** sud = nullptr;
    if ((lines_a != lines_b) || (columns_a != columns_b))
    {
        cout << "���������� ������� ������� ������ ������������\n";
    }
    else
    {
        cout << "�������� ������� A:\n";
        for (int i = 0; i < lines_a; i++)
        {
            for (int j = 0; j < columns_a; j++)
            {
                cout << matrix_a[i][j] << "\t";
            }
            cout << "\n";
        }

        cout << "�������� ������� B:\n";
        for (int i = 0; i < lines_b; i++)
        {
            for (int j = 0; j < columns_b; j++)
            {
                cout << matrix_b[i][j] << "\t";
            }
            cout << "\n";
        }

        sud = new int* [lines_a];
        for (int i = 0; i < lines_a; i++)
        {
            sud[i] = new int[columns_a];
        }

        if (choice5 == 1)
        {

            cout << "������� A-B:\n";
            for (int i = 0; i < lines_a; i++)
            {
                for (int j = 0; j < columns_a; j++)
                {
                   sud[i][j] = matrix_a[i][j] - matrix_b[i][j];
                   cout << sud[i][j] << "\t";
                }
                cout << "\n";
            }
        }
        else if (choice5 == 2)
        {
            cout << "������� B-A:\n";
            for (int i = 0; i < lines_a; i++)
            {
                for (int j = 0; j < columns_a; j++)
                {
                     sud[i][j] = matrix_b[i][j] - matrix_a[i][j];
                     cout << sud[i][j]  << "\t";
                }
                cout << "\n";
            }
        }
    }
    cout << "\n\n\n";
    return  sud;
}

QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_test_lab3.cpp \
    main.cpp

SOURCES += ../equality.cpp\
           ../transposed.cpp\
           ../summ.cpp\
           ../sudtraction.cpp\
           ../composit.cpp\
           ../number.cpp\

HEADERS += \
    tst_test_lab3.h \

HEADERS += ../functions.h\

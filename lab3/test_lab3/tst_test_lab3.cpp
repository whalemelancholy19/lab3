#include <QtTest>
#include "tst_test_lab3.h"
#include "../equality.cpp"
#include "../transposed.cpp"
#include "../summ.cpp"
#include "../sudtraction.cpp"
#include "../composit.cpp"
#include "../number.cpp"

test_lab3::test_lab3()
{

}

void test_lab3:: test_equality()
{
    int tmp1[2][6] = {{4, 8, 1, 3, 9, 5}, {4, 8, 1, 3, 9, 5}};
    int** matrixA;
    matrixA = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixA[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixA[i][j] = tmp1[i][j];
        }
    }

    int tmp2[2][6] = {{5, 3, 1, 3, 10, 5}, {4, 0, 1, -3, 9, 5}};
    int** matrixB;
    matrixB = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixB[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixB[i][j] = tmp2[i][j];
        }
    }

    QCOMPARE(true, equality(matrixA, 2, 6, matrixA, 2, 6));
    QCOMPARE(false, equality(matrixA, 2, 6, matrixB, 2, 6));
}

void test_lab3:: test_transposed()
{
    int tmp1[2][6] = {{4, 8, 1, 3, 9, 5}, {4, 8, 5, -3, 2, 1}};
    int** matrix;
    matrix = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrix[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrix[i][j] = tmp1[i][j];
        }
    }

    int tmp3[6][2] = {{4,4}, {8,8}, {1,5}, {3,-3}, {9,2}, {5,1}};
    int** matrixAfter = new int* [6];
    for (int i = 0; i < 6; i++)
    {
        matrixAfter[i] = new int[2];
    }
    for (int i = 0;i < 6;i++)
    {
        for (int j = 0; j < 2; j++)
        {
           matrixAfter[i][j] = tmp3[i][j];
        }
    }

    int** tran = transp(matrix, 2, 6);
    QCOMPARE(true, equality(matrixAfter, 6, 2, tran, 6, 2));
}
void test_lab3:: test_summ()
{
    int tmp1[2][6] = {{4, 8, 1, 3, 9, 5}, {4, 8, 1, 3, 9, 5}};
    int** matrixA;
    matrixA = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixA[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixA[i][j] = tmp1[i][j];
        }
    }

    int tmp2[2][6] = {{5, 3, 1, 3, 10, 5}, {4, 0, 1, -3, 9, 5}};
    int** matrixB;
    matrixB = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixB[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixB[i][j] = tmp2[i][j];
        }
    }

    int tmp3[2][6] = {{9, 11, 2, 6, 19, 10}, {8, 8, 2, 0, 18, 10}};
    int** matrixAfter = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixAfter[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixAfter[i][j] = tmp3[i][j];
        }
    }

    int** summa = summ(matrixA, 2, 6, matrixB, 2, 6);

    QCOMPARE(true, equality(matrixAfter, 2, 6, summa, 2, 6));
}

void test_lab3:: test_sudt()
{
    int tmp1[2][6] = {{4, 8, 1, 3, 9, 5}, {4, 8, 1, 3, 9, 5}};
    int** matrixA;
    matrixA = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixA[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixA[i][j] = tmp1[i][j];
        }
    }

    int tmp2[2][6] = {{5, 3, 1, 3, 10, 5}, {4, 0, 1, -3, 9, 5}};
    int** matrixB;
    matrixB = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixB[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixB[i][j] = tmp2[i][j];
        }
    }

    int tmp3[2][6] = {{-1, 5, 0, 0, -1, 0}, {0, 8, 0, 6, 0, 0}};
    int** matrixAfter = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixAfter[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixAfter[i][j] = tmp3[i][j];
        }
    }

    int** sud = sudt(matrixA, 2, 6, matrixB, 2, 6, 1);

    QCOMPARE(true, equality(matrixAfter, 2, 6, sud, 2, 6));
}

void test_lab3:: test_composit()
{
       int tmp1[2][6] = {{4, 8, 1, 3, 9, 6}, {4, 8, 1, 3, 9, 7}};
       int** matrixA;
       matrixA = new int* [2];
       for (int i = 0; i < 2; i++)
       {
           matrixA[i] = new int[6];
       }
       for (int i = 0;i < 2;i++)
       {
           for (int j = 0; j < 6; j++)
           {
              matrixA[i][j] = tmp1[i][j];
           }
       }

       int tmp2[6][2] = {{9, 11}, {8, 8}, {4, 5}, {5, 8}, {2, 1}, {7, 2}};
       int** matrixB;
       matrixB = new int* [6];
       for (int i = 0; i < 6; i++)
       {
           matrixB[i] = new int[2];
       }
       for (int i = 0;i < 6;i++)
       {
           for (int j = 0; j < 2; j++)
           {
              matrixB[i][j] = tmp2[i][j];
           }
       }

       int tmp3[2][2] = {{179, 158}, {186, 160}};
       int** matrixAfter = new int* [2];
       for (int i = 0; i < 2; i++)
       {
           matrixAfter[i] = new int[2];
       }
       for (int i = 0;i < 2;i++)
       {
           for (int j = 0; j < 2; j++)
           {
              matrixAfter[i][j] = tmp3[i][j];
           }
       }

       int** com  = composit(matrixA,  2, 6, matrixB, 6, 2);;

       QCOMPARE(true, equality(matrixAfter, 2, 2, com, 2, 2));
}

void test_lab3:: test_number()
{
    int tmp1[2][6] = {{4, 8, 1, 3, 9, 5}, {4, 8, 5, -3, 2, 1}};
    int** matrix;
    matrix = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrix[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrix[i][j] = tmp1[i][j];
        }
    }

    int tmp3[2][6] = {{12, 24, 3, 9, 27, 15}, {12, 24, 15, -9, 6, 3}};
    int** matrixAfter = new int* [2];
    for (int i = 0; i < 2; i++)
    {
        matrixAfter[i] = new int[6];
    }
    for (int i = 0;i < 2;i++)
    {
        for (int j = 0; j < 6; j++)
        {
           matrixAfter[i][j] = tmp3[i][j];
        }
    }

    int** num = number(matrix, 2, 6, 3);

    QCOMPARE(true, equality(matrixAfter, 2, 6, num, 2, 6));
}

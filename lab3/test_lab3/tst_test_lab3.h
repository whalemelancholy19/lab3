#ifndef TEST_LAB3_H
#define TEST_LAB3_H

#include <QtCore>
#include <QtTest/QtTest>

class test_lab3 : public QObject
{
    Q_OBJECT

public:
    test_lab3();

private slots:
    void test_equality();
    void test_transposed();
    void test_summ();
    void test_sudt();
    void test_composit();
    void test_number();
};

#endif // TEST_LAB3_H
